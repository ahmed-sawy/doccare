import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', loadChildren: () => import('./Modules/Guests/guests.module').then(m => m.GuestsModule) },
  { path: 'home', loadChildren: () => import('./Modules/Guests/guests.module').then(m => m.GuestsModule) },
  { path: 'admin', loadChildren: () => import('./Modules/Admin/admin.module').then(m => m.AdminModule) },
  { path: 'doctors', loadChildren: () => import('./Modules/Doctors/doctors.module').then(m => m.DoctorsModule) },
  { path: 'patients', loadChildren: () => import('./Modules/Patients/patients.module').then(m => m.PatientsModule) },

  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
