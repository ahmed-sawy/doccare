import { Injectable } from '@angular/core';
import { IBookingService } from '../Interfaces/IBookingService';
import { Observable } from 'rxjs';
import { BookingDataModel } from 'src/app/Domain/DataModels/BookingDataModel';
import { ApiConfig } from 'src/app/ApiConfig';
import { HttpClient } from '@angular/common/http';
import { SlotModel } from 'src/app/Domain/Models/SlotModel';
import { UserModel } from 'src/app/Domain/Models/UserModel';

@Injectable({
    providedIn: 'root'
})


export class BookingService implements IBookingService {

    private GetAndCalculateDoctorScheduleTimingsURL: string;
    public bookingDataModel: BookingDataModel;
    public selectedDoctor: UserModel;

    constructor(private http: HttpClient, private apiConfig: ApiConfig) {
        this.GetAndCalculateDoctorScheduleTimingsURL = this.apiConfig.baseApiUrl + "Booking/GetDoctorScheduleTimings";
    }
    CheckSlotAvailability(slot: SlotModel): Observable<boolean> {

        return new Observable(oserver => {
            oserver.next(!slot.slotIsBooked);
        });
    }

    GetAndCalculateDoctorScheduleTimings(doctorId: number): Observable<BookingDataModel> {
        return this.http.get<BookingDataModel>(this.GetAndCalculateDoctorScheduleTimingsURL + "?DoctorId=" + doctorId);
    }

}