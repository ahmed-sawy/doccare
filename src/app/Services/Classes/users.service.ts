import { Injectable } from '@angular/core';
import { UserModel } from '../../Domain/Models/UserModel';
import { IUserService } from '../Interfaces/IUserService';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from 'src/app/ApiConfig';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserDataModel } from 'src/app/Domain/DataModels/UserDataModel';

@Injectable({
  providedIn: 'root'
})

export class UsersService implements IUserService {
  public userDataModel: UserDataModel;
  private loginUser: UserModel;

  private getUserURL: string;
  private loginUserURL: string;
  private searchUsersURL: string;

  constructor(private http: HttpClient, private apiConfig: ApiConfig) {

    this.getUserURL = this.apiConfig.baseApiUrl + "User/GetInitObject";
    this.loginUserURL = this.apiConfig.baseApiUrl + "User/LoginUser";
    this.searchUsersURL = this.apiConfig.baseApiUrl + "User/SearchUsers";
  }

  SearchUsers(userData: UserDataModel): Observable<UserDataModel> {
    return this.http.post<UserDataModel>(this.searchUsersURL, userData);
  }

  GetUserDetails(userId: number): Observable<UserModel> {
    throw new Error("Method not implemented.");
  }

  GetUser(): Observable<UserDataModel> {
    return this.http.get<UserDataModel>(this.getUserURL);
  }

  LoginUser(userData: UserDataModel): Observable<UserDataModel> {

    return this.http.post<UserDataModel>(this.loginUserURL, userData);
  }

  GetUserToken() {
    if (sessionStorage.getItem('loginUser') == null) {
      return "";
    }
    this.loginUser = <UserModel>JSON.parse(sessionStorage.getItem('loginUser'));
    return this.loginUser.userToken;
  }

  GetLoginUserInfo() {
    if (sessionStorage.getItem('loginUser') != null) {
      this.loginUser = <UserModel>JSON.parse(sessionStorage.getItem('loginUser'));
      return this.loginUser;
    }
    return null;
  }

  RemoveLogin() {
    sessionStorage.clear();
    location.href = "login";
  }

}
