import { Injectable } from '@angular/core';
import { ICheckoutService } from '../Interfaces/ICheckoutService';
import { Observable } from 'rxjs';
import { CheckoutDataModel } from 'src/app/Domain/DataModels/CheckoutDataModel';
import { PaymentMethodModel } from 'src/app/Domain/Models/PaymentMethodModel';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from 'src/app/ApiConfig';
import { BookingService } from './BookingService';
import { BookingDataModel } from 'src/app/Domain/DataModels/BookingDataModel';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService implements ICheckoutService {

  private GetDoctorPaymentMethodsURL: string;
  private ConfirmAndPayURL: string;


  constructor(private http: HttpClient, private apiConfig: ApiConfig, private bookingService: BookingService) {

    this.GetDoctorPaymentMethodsURL = this.apiConfig.baseApiUrl + "PaymentMethod/GetDoctorPaymentMethods";
    this.ConfirmAndPayURL = this.apiConfig.baseApiUrl + "Booking/ConfirmAndPay"
  }

  GetPaymentMethods(doctorId: number): Observable<PaymentMethodModel[]> {
    return this.http.get<PaymentMethodModel[]>(this.GetDoctorPaymentMethodsURL + "?doctorId=" + doctorId);
  }

  ConfirmAndPay(bookingData: BookingDataModel): Observable<boolean> {
    //TODO Booking proccess
    return this.http.post<boolean>(this.ConfirmAndPayURL, bookingData);
  }


}
