import { Observable } from 'rxjs';
import { BookingDataModel } from 'src/app/Domain/DataModels/BookingDataModel';
import { SlotModel } from 'src/app/Domain/Models/SlotModel';

export interface IBookingService {
    GetAndCalculateDoctorScheduleTimings(doctorId: number): Observable<BookingDataModel>;
    CheckSlotAvailability(slot: SlotModel): Observable<boolean>;
}