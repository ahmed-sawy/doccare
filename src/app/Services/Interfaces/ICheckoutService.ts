import { Observable } from 'rxjs';
import { CheckoutDataModel } from 'src/app/Domain/DataModels/CheckoutDataModel';
import { PaymentMethodModel } from 'src/app/Domain/Models/PaymentMethodModel';

export interface ICheckoutService {
    ConfirmAndPay(checkoutData: any): Observable<boolean>;
    GetPaymentMethods(doctorId: number): Observable<PaymentMethodModel[]>;
}