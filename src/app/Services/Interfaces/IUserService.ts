import { Observable } from 'rxjs';
import { UserDataModel } from 'src/app/Domain/DataModels/UserDataModel';
import { UserModel } from 'src/app/Domain/Models/UserModel';

export interface IUserService {
    GetUser(): Observable<UserDataModel>;
    LoginUser(userData: UserDataModel): Observable<UserDataModel>;
    SearchUsers(userData: UserDataModel): Observable<UserDataModel>;
    GetUserToken();
    GetLoginUserInfo();
    RemoveLogin(): any;
    GetUserDetails(userId: number): Observable<UserModel>;
}