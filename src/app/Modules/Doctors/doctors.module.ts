import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorsRoutingModule } from './doctors-routing.module';
import { DoctorsDashboardComponent } from './doctors-dashboard/doctors-dashboard.component';
import { DoctorsHomeComponent } from './doctors-home/doctors-home.component';
import { DoctorsHeaderComponent } from './doctors-header/doctors-header.component';
import { DoctorsFooterComponent } from './doctors-footer/doctors-footer.component';


@NgModule({
  declarations: [DoctorsDashboardComponent, DoctorsHomeComponent, DoctorsHeaderComponent, DoctorsFooterComponent],
  imports: [
    CommonModule,
    DoctorsRoutingModule
  ]
})
export class DoctorsModule { }
