import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsFooterComponent } from './doctors-footer.component';

describe('DoctorsFooterComponent', () => {
  let component: DoctorsFooterComponent;
  let fixture: ComponentFixture<DoctorsFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
