import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorsDashboardComponent } from './doctors-dashboard/doctors-dashboard.component';
import { DoctorsHomeComponent } from './doctors-home/doctors-home.component';


const routes: Routes = [
  {
    path: '', children: [
      {
        path: '', component: DoctorsHomeComponent, children: [
          { path: '', component: DoctorsDashboardComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule { }
