import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientsHomeComponent } from './patients-home/patients-home.component';


const routes: Routes = [
  { path: '', component: PatientsHomeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule { }
