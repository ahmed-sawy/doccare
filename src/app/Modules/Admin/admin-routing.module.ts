import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminIndexComponent } from './admin-index/admin-index.component';


const routes: Routes = [
  // { path: '', component: AdminIndexComponent },
  // { path: 'home', component: AdminIndexComponent },
  // { path: 'dashboard', component: AdminDashboardComponent, outlet: "adminrout" }

  {
    path: '', children: [
      {
        path: '', component: AdminHomeComponent, children: [
          { path: '', component: AdminDashboardComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
