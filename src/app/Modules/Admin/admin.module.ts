import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminIndexComponent } from './admin-index/admin-index.component';
import { DaysComponent } from './Components/Lookups/days/days.component';
import { SpecialistsComponent } from './Components/Lookups/specialists/specialists.component';

@NgModule({
  declarations: [
    AdminDashboardComponent,
    AdminHomeComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminIndexComponent,
    DaysComponent,
    SpecialistsComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
