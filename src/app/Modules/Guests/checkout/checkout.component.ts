import { Component, OnInit } from '@angular/core';
import { CheckoutService } from 'src/app/Services/Classes/checkout.service';
import { CheckoutDataModel } from 'src/app/Domain/DataModels/CheckoutDataModel';
import { Router } from '@angular/router';
import { BookingService } from 'src/app/Services/Classes/BookingService';
import { BookingDataModel } from 'src/app/Domain/DataModels/BookingDataModel';
import { UsersService } from 'src/app/Services/Classes/users.service';
import { UserModel } from 'src/app/Domain/Models/UserModel';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  checkoutData: CheckoutDataModel;
  bookingData: BookingDataModel;
  loginUser: UserModel;

  constructor(private checkoutService: CheckoutService, private router: Router, private bookingService: BookingService,
    private userService: UsersService) {

    this.loginUser = this.userService.GetLoginUserInfo();
    this.bookingData = this.bookingService.bookingDataModel;

    if (this.loginUser != null) {
      this.bookingData.patient = this.loginUser;
    }

    if (this.bookingData.doctor != null) {
      this.GetPaymentMethods(this.bookingService.selectedDoctor.userId);
    }
  }

  ngOnInit() {
  }

  ConfirmAndPay() {
    console.log("bookingData", this.bookingData);
    this.checkoutService.ConfirmAndPay(this.bookingData)
      .subscribe(results => {
        debugger;
        if (results == true) {
          alert("Booked Successfully");
          // this.router.navigate([""]);
        }
      },
        error => {

        });
  }

  GetPaymentMethods(doctorId: number) {
    this.checkoutService.GetPaymentMethods(this.bookingData.doctor.userId)
      .subscribe(payments => {
        this.bookingData.paymentMethods = payments;
      });
  }

  SelectPaymentMethod(payment) {
    this.bookingData.selectedPaymentMethod = payment;
  }
}
