import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuestsRoutingModule } from './guests-routing.module';
import { GuestIndexComponent } from './guest-index/guest-index.component';
import { GuestHomeComponent } from './guest-home/guest-home.component';
import { GuestHeaderComponent } from './guest-header/guest-header.component';
import { GuestFooterComponent } from './guest-footer/guest-footer.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchDoctorsComponent } from './search-doctors/search-doctors.component';
import { BookingComponent } from './booking/booking.component';
import { CheckoutComponent } from './checkout/checkout.component';


@NgModule({
  declarations: [GuestIndexComponent, GuestHomeComponent, GuestHeaderComponent, GuestFooterComponent, LoginComponent, DashboardComponent, SearchDoctorsComponent, BookingComponent, CheckoutComponent],
  imports: [
    CommonModule,
    GuestsRoutingModule,
    FormsModule
  ]
})
export class GuestsModule { }
