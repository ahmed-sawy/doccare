import { Component, OnInit } from '@angular/core';
import { IUserService } from 'src/app/Services/Interfaces/IUserService';
import { UsersService } from 'src/app/Services/Classes/users.service';
import { UserDataModel } from 'src/app/Domain/DataModels/UserDataModel';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userData: UserDataModel;
  public loginBtnDisabled: boolean;
  public loginTxt: string;

  constructor(private userService: UsersService, private route: ActivatedRoute, private router: Router) {

    // this line to get userData from router (using resolver)
    this.userData = this.route.snapshot.data.items;

    console.log("userData", this.userData);

    this.loginBtnDisabled = false;
    this.loginTxt = "Login";
  }

  ngOnInit() {
  }

  loginUser() {

    this.loginBtnDisabled = true;
    this.loginTxt = "Login...";

    this.userService.LoginUser(this.userData)
      .subscribe(userData => {
        this.userData = userData;
        sessionStorage.setItem('loginUser', JSON.stringify(this.userData.model));
        this.router.navigate(["/home"]);

        console.log("userData is ", this.userData);

        this.loginBtnDisabled = false;
        this.loginTxt = "Login";
      },
        error => {
          this.loginBtnDisabled = false;
          this.loginTxt = "Login";
        });
  }

}
