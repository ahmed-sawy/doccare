import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestIndexComponent } from './guest-index/guest-index.component';
import { GuestHomeComponent } from './guest-home/guest-home.component';
import { LoginComponent } from './login/login.component';
import { LoginGuard } from 'src/app/Guards/login.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthenticateGuard } from 'src/app/Guards/authenticate.guard';
import { UsersResolver } from 'src/app/Resolvers/users.resolver';
import { SearchDoctorsComponent } from './search-doctors/search-doctors.component';
import { BookingComponent } from './booking/booking.component';
import { CheckoutComponent } from './checkout/checkout.component';


const routes: Routes = [
  {
    path: '', children: [
      {
        path: '', component: GuestIndexComponent, children: [
          { path: '', component: GuestHomeComponent, resolve: { items: UsersResolver } },
          { path: 'login', component: LoginComponent, resolve: { items: UsersResolver }, canActivate: [LoginGuard] },
          { path: 'patient-dashboard', component: DashboardComponent, canActivate: [AuthenticateGuard], data: { moduleId: 3 } },
          { path: 'search', component: SearchDoctorsComponent },
          { path: 'booking', component: BookingComponent },
          { path: 'checkout', component: CheckoutComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestsRoutingModule { }
