import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/Services/Classes/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataModel } from 'src/app/Domain/DataModels/UserDataModel';

enum enUserType {
  doctor = 2,
  patient = 3,
}

@Component({
  selector: 'app-guest-home',
  templateUrl: './guest-home.component.html',
  styleUrls: ['./guest-home.component.css']
})
export class GuestHomeComponent implements OnInit {

  public userDataModel: UserDataModel;

  constructor(private userService: UsersService, private route: ActivatedRoute, private router: Router) {
    this.userDataModel = this.route.snapshot.data.items;

    console.log("userDataModel", this.userDataModel);
  }

  ngOnInit() {
  }

  searchUsers() {
    this.userDataModel.searchCrit.userType = enUserType.doctor;

    this.userService.SearchUsers(this.userDataModel)
      .subscribe(userDataModel => {
        // this.userDataModel = userDataModel;
        this.userService.userDataModel = userDataModel;
        this.router.navigate(["/search"]);
      },
        error => {
          console.log("Error");
        }
      );
  }
}
