import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/Services/Classes/BookingService';
import { BookingDataModel } from 'src/app/Domain/DataModels/BookingDataModel';
import { SlotModel } from 'src/app/Domain/Models/SlotModel';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ScheduleTimingModel } from 'src/app/Domain/Models/ScheduleTimingModel';
import { UserModel } from 'src/app/Domain/Models/UserModel';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  bookingData: BookingDataModel;
  isAvailable: boolean;
  notBookedSlotClass: string;
  selectedDoctor: UserModel;

  constructor(private bookingService: BookingService, private router: Router) {

    this.selectedDoctor = this.bookingService.selectedDoctor;
    // this.selectedDoctor.
    this.GetAndCalculateDoctorScheduleTimings();
  }

  ngOnInit() {

  }

  GetAndCalculateDoctorScheduleTimings() {
    this.bookingService.GetAndCalculateDoctorScheduleTimings(this.bookingService.selectedDoctor.userId)
      .subscribe(bookingData => {
        this.bookingData = bookingData;
        this.bookingService.bookingDataModel = this.bookingData;
        this.notBookedSlotClass = "timing";

        console.log("Booking Data is", this.bookingData);
      }, error => {
        console.log("Error");
      });
  }

  CheckSlotAvailability(dayIndex: number, slotIndex: number) {

    let schedule: ScheduleTimingModel = this.bookingData.scheduleTimings[dayIndex];
    let slot: SlotModel = this.bookingData.scheduleTimings[dayIndex].slots[slotIndex];

    this.bookingService.CheckSlotAvailability(slot)
      .subscribe(isAvailable => {
        this.isAvailable = isAvailable;
        if (this.isAvailable) {
          debugger;

          let elementId = "SlotClass" + dayIndex + slotIndex;
          var element = document.getElementById(elementId);

          if (element.className === "timing selected") {
            element.className = "timing";
            this.bookingService.bookingDataModel.selectedSlot = null;
            this.bookingService.bookingDataModel.selectedScheduleTiming = null;
          }
          if (element.className === "timing") {
            element.className = "timing selected";
            this.bookingService.bookingDataModel.selectedSlot = slot;
            this.bookingService.bookingDataModel.selectedScheduleTiming = schedule;
          }

          // if (this.notBookedSlotClass + dayIndex + slotIndex === "timing selected") {
          //   this.notBookedSlotClass + dayIndex + slotIndex = "timing";
          //   this.bookingService.bookingDataModel.selectedSlot = null;
          //   this.bookingService.bookingDataModel.selectedScheduleTiming = null;
          // }
          // if (this.notBookedSlotClass + dayIndex + slotIndex === "timing") {
          //   this.notBookedSlotClass + dayIndex + slotIndex = "timing selected";
          //   this.bookingService.bookingDataModel.selectedSlot = slot;
          //   this.bookingService.bookingDataModel.selectedScheduleTiming = schedule;
          // }
        }
        else {
          alert("Slot is not Available");
          // console.log("Slot is not Available");
        }
      },
        error => {

        });
  }

  ProceedToPay() {
    if (this.bookingService.bookingDataModel.selectedSlot != null && this.bookingService.bookingDataModel.doctor != null) {
      this.router.navigate(["checkout"]);
    }
    else {
      alert("Please select timing to book.");
    }
  }
}
