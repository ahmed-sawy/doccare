import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/Services/Classes/users.service';
import { UserModel } from 'src/app/Domain/Models/UserModel';

@Component({
  selector: 'app-guest-header',
  templateUrl: './guest-header.component.html',
  styleUrls: ['./guest-header.component.css']
})
export class GuestHeaderComponent implements OnInit {
  public loggedInUser: UserModel;

  constructor(private userService: UsersService) {

    this.loggedInUser = this.userService.GetLoginUserInfo();
    console.log("loggedInUser", this.loggedInUser);
  }

  ngOnInit() {
  }

  logout() {
    this.userService.RemoveLogin();
  }
}
