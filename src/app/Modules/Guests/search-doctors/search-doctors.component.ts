import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/Services/Classes/users.service';
import { UserDataModel } from 'src/app/Domain/DataModels/UserDataModel';
import { Router } from '@angular/router';
import { BookingService } from 'src/app/Services/Classes/BookingService';
import { UserModel } from 'src/app/Domain/Models/UserModel';

@Component({
  selector: 'app-search-doctors',
  templateUrl: './search-doctors.component.html',
  styleUrls: ['./search-doctors.component.css']
})
export class SearchDoctorsComponent implements OnInit {
  public userDataModel: UserDataModel;

  constructor(private userService: UsersService, private router: Router, private bookingService: BookingService) {
    this.userDataModel = this.userService.userDataModel;
    console.log("userDataModel", this.userDataModel);
  }

  ngOnInit() {
  }

  BookAppointment(doctor) {
    this.bookingService.selectedDoctor = doctor;
    this.router.navigate(["booking"]);
  }

}
