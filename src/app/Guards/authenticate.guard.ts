import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from '../Services/Classes/users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateGuard implements CanActivate {

  constructor(private userService: UsersService, private route: Router, private activateRoute: ActivatedRoute) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let componentModule = next.data;
    let loggedInUser = this.userService.GetLoginUserInfo();

    if (loggedInUser != null) {
      if (loggedInUser.userTypeId == componentModule.moduleId) {
        return true;
      }
      else {
        this.route.navigate(["/home"]);
        return false;
      }
    }
    else {
      return false;
    }
  }

}
