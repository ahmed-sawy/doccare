import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';
import { UsersService } from '../Services/Classes/users.service';

@Injectable()
export class UsersResolver implements Resolve<any> {

    constructor(private userService: UsersService) { }

    resolve() {

        console.log("Hi I'm User Resolver");
        return this.userService.GetUser();
    }

}