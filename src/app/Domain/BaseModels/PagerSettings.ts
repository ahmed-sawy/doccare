export interface PagerSettings {
    pagesCount: number;
    totalRecords: number;
    currentPage: number;
    selectedRecsPerPage: number;
    pages: number[];
}