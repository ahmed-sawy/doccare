export interface UserSearch {
    userName: string;
    specialist: string;
    hospital: string;
    userType: number;
}