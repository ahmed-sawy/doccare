export interface UserTypeModel {
    userTypeId: number;
    userTypeName: string;
    userTypeDesc: string;
    userTypeIsActive: boolean;
    hospitalId: number;
}