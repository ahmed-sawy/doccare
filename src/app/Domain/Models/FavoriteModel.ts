import { UserModel } from './UserModel';

export interface FavoriteModel {
    patientId: number;
    doctorId: number;

    patient: UserModel;
    doctor: UserModel;
}