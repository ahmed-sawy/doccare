export interface PaymentMethodModel {
    paymentId: number;
    paymentName: string;
    paymentIsActive: boolean;
}