export interface ReviewModel {
    reviewId: number;
    reviewTitle: string;
    reviewDetails: string;
    reviewStars: number;
    reviewDate: Date;
    reviewIsActive: boolean;
    reviewParent: number; //if = 0 it means that is main review if greater than 0 it means sub review (Replay)
    patientId: number;
    doctorId: number;


}