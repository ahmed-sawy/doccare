export class SpecialistModel {
    specialistId: number;
    specialistName: string;
    specialistDesc: string;
    specialistIsActive: boolean;
    hospitalId: number;


}