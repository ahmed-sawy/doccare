export interface HospitalModel {
    hospitalId: number;
    hospitalName: string;
    hospitalAddress: string;
    hospitalPhone1: string;
    hospitalPhone2: string;
    hospitalEmail: string;
    hospitalIsActive: boolean;


}