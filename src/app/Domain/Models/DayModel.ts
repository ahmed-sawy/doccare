export interface DayModel {
    dayId: number;
    dayName: string;
    dayDesc: string;
    dayIsActive: boolean;
}