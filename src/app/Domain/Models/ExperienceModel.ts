import { UserModel } from './UserModel';

export interface ExperienceModel {
    experienceId: number;
    experienceName: string;
    experienceDesc: string;
    experienceFrom: Date;
    experienceTo: Date;
    doctorId: number;

    doctor: UserModel;
}