export interface SocialMediaModel{
    socialMediaId: number;
    faceBook: string;
    twitter: string;
    instagram: string;
    pinterest: string;
    linkedIn: string;
    youtube: string;
    doctorId: number;
}