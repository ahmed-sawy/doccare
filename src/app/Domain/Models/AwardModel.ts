import { UserModel } from './UserModel';

export interface AwardModel {
    awardId: number;
    awards: string;
    awardYear: Date;
    doctorId: number;

    doctor: UserModel;
}