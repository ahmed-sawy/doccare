export interface InvoiceStatusModel {
    invoiceStatusId: number;
    invoiceStatusName: string;
    invoiceStatusDesc: string;
    invoiceStatusIsActive: boolean;
    hospitalId: number;


}