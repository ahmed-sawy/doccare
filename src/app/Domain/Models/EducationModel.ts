import { UserModel } from './UserModel';

export interface EducationModel {
    educationId: number;
    educationDegree: string;
    educationCollege: string;
    educationCompletionYear: Date;
    doctorId: number;

    doctor: UserModel;
}