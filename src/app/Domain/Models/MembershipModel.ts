import { UserModel } from './UserModel';

export interface MembershipModel {
    membershipId: number;
    memberships: string;
    doctorId: number;

    doctor: UserModel;
}