import { Time } from '@angular/common';
import { SlotModel } from './SlotModel';

export interface ScheduleTimingModel {
    scheduleId: number;
    scheduleFrom: Time;
    scheduleTo: Time;
    appointmentsNo: number;
    doctorId: number;
    dayId: number;

    slots: SlotModel;
}