export interface GenderModel {
    genderId: number;
    genderName: string;
    genderIsActive: boolean;
}