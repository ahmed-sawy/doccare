import { AppointmentModel } from './AppointmentModel';
import { AwardModel } from './AwardModel';
import { EducationModel } from './EducationModel';
import { ExperienceModel } from './ExperienceModel';
import { FavoriteModel } from './FavoriteModel';
import { MembershipModel } from './MembershipModel';
import { RegistrationModel } from './RegistrationModel';
import { SocialMediaModel } from './SocialMediaModel';

export interface UserModel {

    userId: number;
    userCode: string;
    userTitle: string;
    userFirstName: string;
    userLastName: string;
    userEmail: string;
    userPhone: string;
    userPass: string;
    userIsActive: boolean;
    userTypeId: number;
    hospitalId: number;
    genderId: number;
    bloodGroupId: number;
    userToken: string;

    appointments: AppointmentModel[];
    awards: AwardModel[];
    educations: EducationModel[];
    experiences: ExperienceModel[];
    favorites: FavoriteModel[];
    memberships: MembershipModel[];
    registrations: RegistrationModel[];
    socialMedia: SocialMediaModel;
}