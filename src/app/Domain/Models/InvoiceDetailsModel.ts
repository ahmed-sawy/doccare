export interface InvoiceDetailsModel {
    invServiceId: number;
    invServicePrice: number;
    invServiceDiscount: number;
    invServiceTotalPrice: number;
    invServiceQuntity: number;
    serviceId: number;
    invoiceId: number;


}