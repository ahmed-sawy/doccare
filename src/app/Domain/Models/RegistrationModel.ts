export interface RegistrationModel{
    registrationId: number;
    registrations: string;
    registrationYear: Date;
    doctorId: number;
}