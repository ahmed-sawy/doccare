export interface ServiceModel {
    serviceId: number;
    serviceName: string;
    serviceDesc: string;
    serviceIsActive: boolean;
    specialistId: number;


}