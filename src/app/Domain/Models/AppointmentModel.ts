import { Time } from '@angular/common';
import { UserModel } from './UserModel';
import { InvoiceModel } from './InvoiceModel';

export interface AppointmentModel {

    appointmentId: number;
    appointmentBookDate: Date;
    appointmentDate: Date;
    appointmentFrom: Time;
    appointmentTo: Time;
    appointmentAmount: number;
    patientId: number;
    doctorId: number;
    invoiceId: number;
    patientTypeId: number;

    patient: UserModel;
    doctor: UserModel;
    invoice: InvoiceModel;
}