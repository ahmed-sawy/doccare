export interface SlotDurationModel {
    slotDurationId: number;
    slotDurationName: string;
    slotDurationDesc: string;
    slotDurationValue: number;
    slotDurationIsActive: boolean;
    hospitalId: number;


}