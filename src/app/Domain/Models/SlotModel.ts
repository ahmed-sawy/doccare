import { Time } from '@angular/common';

export interface SlotModel {
    slotId: number;
    slotTimeFrom: Time;
    slotTimeTo: Time;
    slotIsBooked: boolean;
    scheduleId: number;


}