import { UserModel } from './UserModel';
import { ServiceModel } from './ServiceModel';

export interface DocServicePriceModel {
    doctorId: number;
    serviceId: number;
    servicePrice: number;

    doctor: UserModel;
    service: ServiceModel;
}