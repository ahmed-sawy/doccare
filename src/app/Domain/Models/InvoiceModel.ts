export interface InvoiceModel {
    invoiceId: number;
    invoiceNo: string;
    invoiceAmount: number;
    invoiceDiscount: number;
    invoiceTotalAmount: number;
    invoiceIssueDate: Date;
    invoicePaidDate: Date;
    invoiceInfo: string;
    doctorId: number;
    patientId: number;
    statusId: number;
    paymentId: number;
    appointmentId: number;

}