import { UserModel } from '../Models/UserModel';
import { ScheduleTimingModel } from '../Models/ScheduleTimingModel';
import { SlotModel } from '../Models/SlotModel';
import { PaymentMethodModel } from '../Models/PaymentMethodModel';

export interface BookingDataModel {
    doctor: UserModel;
    patient: UserModel;
    selectedSlot: SlotModel;
    selectedScheduleTiming: ScheduleTimingModel;
    selectedPaymentMethod: PaymentMethodModel;

    paymentMethods: PaymentMethodModel[];
    scheduleTimings: ScheduleTimingModel[];

    availableDaysNo: number;
}