import { PagerSettings } from '../BaseModels/PagerSettings';
import { UserSearch } from '../SearchModels/UserSearch';
import { UserModel } from '../Models/UserModel';

export interface UserDataModel {
    pagerSettings: PagerSettings;
    searchCrit: UserSearch;
    model: UserModel;
    opResultCode: number;
    opResultMsg: string;
    searchResults: UserModel[];
}